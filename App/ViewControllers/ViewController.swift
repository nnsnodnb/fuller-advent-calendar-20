//
//  ViewController.swift
//  App
//
//  Created by Oka Yuya on 2020/12/13.
//

import RxCocoa
import RxSwift
import UIKit

final class ViewController: UIViewController {

    // MARK: - Properties
    private let viewModel: ViewModelType
    private let disposeBag = DisposeBag()

    @IBOutlet private weak var textView: UITextView! {
        didSet {
            textView.rx.text.orEmpty.changed.asSignal().emit(to: viewModel.inputs.text).disposed(by: disposeBag)
        }
    }
    @IBOutlet private weak var textViewBottomConstraint: NSLayoutConstraint!

    private lazy var rightBarButtonItem: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "送信", style: .plain, target: nil, action: nil)
        navigationItem.rightBarButtonItem = barButtonItem
        return barButtonItem
    }()

    // MARK: - Initialize
    init(viewModel: ViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: "ViewController", bundle: .main)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // 送信ボタン
        rightBarButtonItem.rx.tap.asSignal().emit(to: viewModel.inputs.post).disposed(by: disposeBag)
        viewModel.outputs.isSendButtonEnabled.drive(rightBarButtonItem.rx.isEnabled).disposed(by: disposeBag)

        // 投稿完了
        viewModel.outputs.didCompleted.map { _ in nil }.drive(textView.rx.text).disposed(by: disposeBag)

        // キーボード
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
            .compactMap { $0.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue }
            .compactMap { $0.cgRectValue.height }
            .asDriver(onErrorDriveWith: .empty())
            .map { [unowned self] in $0 - self.view.safeAreaInsets.bottom + 8 }
            .drive(textViewBottomConstraint.rx.constant)
            .disposed(by: disposeBag)
        NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)
            .map { _ in 8 }
            .asDriver(onErrorDriveWith: .empty())
            .drive(textViewBottomConstraint.rx.constant)
            .disposed(by: disposeBag)
    }
}

extension ViewController {

    static func make() -> ViewController {
        let viewModel = ViewModel()
        return .init(viewModel: viewModel)
    }
}
