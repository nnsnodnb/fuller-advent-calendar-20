//
//  ViewModel.swift
//  App
//
//  Created by Oka Yuya on 2020/12/13.
//

import Action
import Foundation
import RxCocoa
import RxSwift

protocol ViewModelInput: AnyObject {

    var text: PublishRelay<String> { get }
    var post: PublishRelay<Void> { get }
}

protocol ViewModelOutput: AnyObject {

    var isSendButtonEnabled: Driver<Bool> { get }
    var didCompleted: Driver<Void> { get }
}

protocol ViewModelType: AnyObject {

    var inputs: ViewModelInput { get }
    var outputs: ViewModelOutput { get }
}

final class ViewModel: ViewModelType {

    // MARK: - Inputs Sources
    let text: PublishRelay<String> = .init()
    let post: PublishRelay<Void> = .init()
    // MARK: - Outputs Sources
    let isSendButtonEnabled: Driver<Bool>
    let didCompleted: Driver<Void>
    // MARK: - Properties
    var inputs: ViewModelInput { return self }
    var outputs: ViewModelOutput { return self }

    private let disposeBag = DisposeBag()
    private let postText: BehaviorRelay<String> = .init(value: "")
    private let postAction: Action<String, Void> = .init { _ in .just(()) }

    // MARK: - Initialize
    init() {
        self.isSendButtonEnabled = Observable.combineLatest(
            postText.map { !$0.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && $0.count <= 200 },
            postAction.executing.map { !$0 }
        ) { $0 && $1 }
        .distinctUntilChanged()
        .asDriver(onErrorDriveWith: .empty())
        self.didCompleted = postAction.elements.asDriver(onErrorDriveWith: .empty())

        text.bind(to: postText).disposed(by: disposeBag)
        post.asObservable()
            .withLatestFrom(isSendButtonEnabled.asObservable()) { $1 }
            .filter { $0 }
            .withLatestFrom(postText.asObservable()) { $1 }
            .bind(to: postAction.inputs)
            .disposed(by: disposeBag)
    }
}

// MARK: - ViewModelInput
extension ViewModel: ViewModelInput {}

// MARK: - ViewModelOutput
extension ViewModel: ViewModelOutput {}
