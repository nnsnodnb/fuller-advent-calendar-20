//
//  StringExtension.swift
//  AppTests
//
//  Created by Oka Yuya on 2020/12/13.
//

import Foundation

extension String {

    static func createRandomCharacters(with length: Int) -> String {
        let base: [Character] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".map { $0 }
        let text: String = (0..<length).map { _ in "\(base.randomElement()!)" }.joined()
        return text
    }
}
