//
//  ViewModelIsSendButtonEnabledSpec.swift
//  AppTests
//
//  Created by Oka Yuya on 2020/12/13.
//

import Nimble
import Quick
import RxSwift
import RxTest
@testable import App

final class ViewModelIsSendButtonEnabledSpec: QuickSpec {

    override func spec() {
        var disposeBag: DisposeBag!
        var testScheduler: TestScheduler!

        beforeEach {
            disposeBag = .init()
            testScheduler = .init(initialClock: 0)
        }
        afterEach {
            disposeBag = nil
            testScheduler = nil
        }

        describe("送信ボタンの状態") {
            it("何も入力されていないので無効になる") {
                let viewModel = ViewModel()
                let observer = testScheduler.createObserver(Bool.self)

                viewModel.outputs.isSendButtonEnabled.drive(observer).disposed(by: disposeBag)

                expect(observer.events) == [.next(0, false)]
            }

            it("改行のみの入力なので無効のまま") {
                let viewModel = ViewModel()
                let observer = testScheduler.createObserver(Bool.self)

                viewModel.outputs.isSendButtonEnabled.drive(observer).disposed(by: disposeBag)
                viewModel.inputs.text.accept("\n")

                expect(observer.events) == [.next(0, false)]
            }

            it("スペースのみの入力なので無効のまま") {
                let viewModel = ViewModel()
                let observer = testScheduler.createObserver(Bool.self)

                viewModel.outputs.isSendButtonEnabled.drive(observer).disposed(by: disposeBag)
                viewModel.inputs.text.accept(" ")

                expect(observer.events) == [.next(0, false)]
            }

            it("200文字を越えたので無効にする") {
                let viewModel = ViewModel()
                let observer = testScheduler.createObserver(Bool.self)

                viewModel.outputs.isSendButtonEnabled.drive(observer).disposed(by: disposeBag)
                let text: String = .createRandomCharacters(with: 200)
                viewModel.inputs.text.accept(text)
                viewModel.inputs.text.accept(text + " ")

                expect(observer.events) == [.next(0, false), .next(0, true), .next(0, false)]
            }

            it("送信ボタンが押されて完了するまで無効になる") {
                let viewModel = ViewModel()
                let observer = testScheduler.createObserver(Bool.self)

                viewModel.outputs.isSendButtonEnabled.drive(observer).disposed(by: disposeBag)
                viewModel.inputs.text.accept("投稿するよぉぉ〜〜")
                viewModel.inputs.post.accept(())

                expect(observer.events) == [.next(0, false), .next(0, true), .next(0, false), .next(0, true)]
            }
        }
    }
}
